#ifndef _DIONE_DEBUG_H_
#define _DIONE_DEBUG_H_
#include <stdio.h>
#define DIONE_LOG(...) printf(__VA_ARGS__)


//#define CONFIG_MQTT_LOG_ERROR_ON
//#define CONFIG_MQTT_LOG_WARN_ON
#define CONFIG_MQTT_LOG_INFO_ON

#endif
