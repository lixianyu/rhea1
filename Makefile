#
# This is a project Makefile. It is assumed the directory this Makefile resides in is a
# project subdirectory.
#

PROJECT_NAME := Rhea

include $(IDF_PATH)/make/project.mk

#COMPONENT_ADD_INCLUDEDIRS := components/include
CFLAGS += -Wno-error=char-subscripts
#-Wno-error=unused-function \
#CPPFLAGS += -DMQTT_SERVER_DIONE -DLXY
#CPPFLAGS += -Werror=char-subscripts
