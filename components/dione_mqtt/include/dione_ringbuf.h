#ifndef _DIONE_RING_BUF_H_
#define _DIONE_RING_BUF_H_

#include <stdint.h>


typedef struct{
  uint8_t* p_o;        /**< Original pointer */
  uint8_t* volatile p_r;   /**< Read pointer */
  uint8_t* volatile p_w;   /**< Write pointer */
  volatile int32_t fill_cnt;  /**< Number of filled slots */
  int32_t size;       /**< Buffer size */
  int32_t block_size;
}DIONE_RINGBUF;

int32_t dione_rb_init(DIONE_RINGBUF *r, uint8_t* buf, int32_t size, int32_t block_size);
int32_t dione_rb_put(DIONE_RINGBUF *r, uint8_t* c);
int32_t dione_rb_get(DIONE_RINGBUF *r, uint8_t* c);
int32_t dione_rb_available(DIONE_RINGBUF *r);
uint32_t dione_rb_read(DIONE_RINGBUF *r, uint8_t *buf, int len);
uint32_t dione_rb_write(DIONE_RINGBUF *r, uint8_t *buf, int len);

#endif
