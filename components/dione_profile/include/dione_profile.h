#ifndef __DIONE_PROFILE_H__
#define __DIONE_PROFILE_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * DEFINES
 ****************************************************************************************
 */

#define DIONE_HT_MEAS_MAX_LEN            (64)

#define DIONE_MANDATORY_MASK             (0x0F)
#define DIONE_BODY_SENSOR_LOC_MASK       (0x30)
#define DIONE_HR_CTNL_PT_MASK            (0xC0)


///Attributes State Machine
enum
{
    DIONE_IDX_SVC,

    DIONE_IDX_HR_MEAS_CHAR,
    DIONE_IDX_HR_MEAS_VAL,
    DIONE_IDX_HR_MEAS_NTF_CFG,

    DIONE_IDX_BOBY_SENSOR_LOC_CHAR,
    DIONE_IDX_BOBY_SENSOR_LOC_VAL,
#if 0
    DIONE_IDX_HR_CTNL_PT_CHAR,
    DIONE_IDX_HR_CTNL_PT_VAL,
#endif
    DIONE_IDX_NB,
};

extern void dione_profile_init(void);
#endif

